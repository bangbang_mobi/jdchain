### JD Chain 源代码库

#### project

- URL：git@github.com:blockchain-jd-com/jdchain-project.git
- 说明：公共的父项目，定义公共的依赖

#### framework 

- URL：git@github.com:blockchain-jd-com/jdchain-framework.git
- 说明：框架源码库，定义公共数据类型、框架、模块组件接口、`SDK`、`SPI`、工具

#### core

- URL：git@github.com:blockchain-jd-com/jdchain-core.git
- 说明：模块组件实现的源码库

#### explorer

- URL：git@github.com:blockchain-jd-com/explorer.git
- 说明：相关产品的前端模块的源码库

#### bft-smart

- URL：git@github.com:blockchain-jd-com/bftsmart.git
- 说明：`BFT-SMaRt` 共识算法的源码库

#### explorer

- URL：git@github.com:blockchain-jd-com/explorer.git
- 说明：区块链浏览器/管理工具前端代码

#### binary-proto

- URL：git@github.com:blockchain-jd-com/binary-proto.git
- 说明：自研序列化/反序列化框架

#### utils

- URL：git@github.com:blockchain-jd-com/utils.git
- 说明：工具类库

#### httpservice

- URL：git@github.com:blockchain-jd-com/httpservice.git
- 说明：`HTTP` `RPC` 服务框架

#### kvdb

- URL：git@github.com:blockchain-jd-com/kvdb.git
- 说明：基于`RocksDB` `JNI`方式实现可独立部署，支持`KV`读写的数据库服务

#### test

- URL：git@github.com:blockchain-jd-com/jdchain-test.git
- 说明：集成测试用例的源码库